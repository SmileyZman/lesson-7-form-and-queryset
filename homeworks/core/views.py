# from django.shortcuts import render
from core.forms import GroupCreateForm, StudentCreateForm, TeacherCreateForm
from core.models import Group

from django.db.models.aggregates import Avg, Count, Max, Min
from django.views.generic import FormView, TemplateView


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        groups = Group.objects.all().annotate(
            count_student=Count('students'),
            avg_age_student=Avg('students__age'),
            min_age_student=Min('students__age'),
            max_age_student=Max('students__age'),
        ).prefetch_related('teachers')
        context = {
            "groups": groups
        }
        return context


class StudentCreateView(FormView):
    template_name = "student_create.html"
    form_class = StudentCreateForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return super(StudentCreateView, self).form_valid(form)


class GroupCreateView(FormView):
    template_name = "group_create.html"
    form_class = GroupCreateForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return super(GroupCreateView, self).form_valid(form)


class TeacherCreateView(FormView):
    template_name = "teacher_create.html"
    form_class = TeacherCreateForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return super(TeacherCreateView, self).form_valid(form)
