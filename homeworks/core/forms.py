from core.models import Group, Student, Teacher

from django import forms


class StudentCreateForm(forms.ModelForm):

    class Meta:
        model = Student
        fields = "__all__"
        widgets = {
            'group': forms.widgets.RadioSelect()
        }


class GroupCreateForm(forms.ModelForm):

    class Meta:
        model = Group
        fields = "__all__"


class TeacherCreateForm(forms.ModelForm):

    class Meta:
        model = Teacher
        fields = "__all__"
        widgets = {
            'group': forms.widgets.RadioSelect()
        }
