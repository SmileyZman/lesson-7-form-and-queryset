from django.db import models


class Student(models.Model):
    name = models.CharField(max_length=255, verbose_name="first name")
    last_name = models.CharField(max_length=255)
    group = models.ForeignKey("core.Group", on_delete=models.CASCADE,
                              related_name='students')
    age = models.IntegerField()

    def __str__(self):
        return self.name


class Group(models.Model):
    name = models.CharField(max_length=255, verbose_name="group name")

    def __str__(self):
        return self.name


class Teacher(models.Model):
    name = models.CharField(max_length=255)
    group = models.ForeignKey("core.Group", on_delete=models.CASCADE,
                              related_name='teachers')

    def __str__(self):
        return self.name
